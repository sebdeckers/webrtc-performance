const startCaptureButton = document.querySelector('#start-capture')
const stopCaptureButton = document.querySelector('#stop-capture')
const startStreamButton = document.querySelector('#start-stream')
const stopStreamButton = document.querySelector('#stop-stream')
const loopbackVideo = document.querySelector('#loopback video')
const localVideo = document.querySelector('#local video')
const loopbackHeader = document.querySelector('#loopback header')
const localHeader = document.querySelector('#local header')
let loopbackStream
let local
let remote
let loopbackPoll
let localPoll

startCaptureButton.addEventListener('click', async (event) => {
  getScreenId(async (error, sourceId, constraints) => {
    startCaptureButton.disabled = true
    stopCaptureButton.disabled = false

    const width = document.querySelector('#width ~ input').value
    const height = document.querySelector('#height ~ input').value
    const framerate = document.querySelector('#framerate ~ input').value

    if (width) {
      constraints.video.mandatory.minWidth = width
      constraints.video.mandatory.maxWidth = width
    }
    if (height) {
      constraints.video.mandatory.minHeight = height
      constraints.video.mandatory.maxHeight = height
    }
    if (framerate) {
      constraints.video.mandatory.minFrameRate = framerate
      constraints.video.mandatory.maxFrameRate = framerate
    }

    // constraints.video.mandatory.chromeMediaSource = 'desktop'
    // delete constraints.video.mandatory.chromeMediaSource
    // delete constraints.video.mandatory.chromeMediaSourceId

    loopbackStream = await navigator.mediaDevices.getUserMedia(constraints)
    loopbackVideo.srcObject = loopbackStream
    loopbackPoll = pollVideoStats(loopbackVideo, loopbackHeader)
    startStreamButton.disabled = false
  })
})

stopCaptureButton.addEventListener('click', async (event) => {
  startStreamButton.disabled = true
  stopCaptureButton.disabled = true
  startCaptureButton.disabled = false

  for (const track of loopbackStream.getTracks()) track.stop()
  loopbackStream = undefined
  loopbackVideo.srcObject = undefined
  loopbackHeader.textContent = ''
  clearInterval(loopbackPoll)
})

startStreamButton.addEventListener('click', async (event) => {
  startStreamButton.disabled = true
  stopStreamButton.disabled = false

  local = new RTCPeerConnection(null)
  remote = new RTCPeerConnection(null)

  local.addEventListener('icecandidate', ({candidate}) => {
    remote.addIceCandidate(candidate)
  }, {once: true})
  remote.addEventListener('icecandidate', ({candidate}) => {
    local.addIceCandidate(candidate)
  }, {once: true})

  remote.addEventListener('addstream', ({stream}) => {
    console.log('Stream received', stream)
    localVideo.srcObject = stream
    localPoll = pollVideoStats(localVideo, localHeader)
  }, {once: true})

  local.addStream(loopbackStream)

  const offer = await local.createOffer({
    offerToReceiveAudio: false,
    offerToReceiveVideo: true
  })

  const coding = document.querySelector('#coding + input').value
  const rtpmap = new Map([
    ['VP8', 96],
    ['VP9', 98],
    ['H.264', 100]
  ])

  if (coding) {
    if (rtpmap.has(coding)) {
      offer.sdp = offer.sdp.split('\r\n').map((line) => {
        if (line.startsWith('m=video')) {
          const preferred = rtpmap.get(coding)
          const fallback = Array
            .from(rtpmap.values())
            .filter((number) => number !== preferred)
            .join(' ')
          return `m=video 9 UDP/TLS/RTP/SAVPF ${preferred} ${fallback} 102 127 97 99 101 125`
        } else return line
      }).join('\r\n')
    } else throw new Error(`"${coding}" is not supported 💩`)
  }

  local.setLocalDescription(offer)
  remote.setRemoteDescription(offer)

  const answer = await remote.createAnswer()

  local.setRemoteDescription(answer)
  remote.setLocalDescription(answer)
})

stopStreamButton.addEventListener('click', async (event) => {
  stopStreamButton.disabled = true
  startStreamButton.disabled = stopCaptureButton.disabled

  localVideo.srcObject = undefined
  localHeader.textContent = ''
  clearInterval(localPoll)

  local.close()
  remote.close()
})

function pollVideoStats (video, output) {
  let lastFrames = 0
  let lastTime = 0
  return setInterval(() => {
    const nowTime = Date.now()
    const nowFrames = video.mozPaintedFrames || video.webkitDecodedFrameCount

    const duration = nowTime - lastTime
    const frames = nowFrames - lastFrames
    const framerate = frames / (duration / 1000)
    output.textContent = `${video.videoWidth}x${video.videoHeight}@${framerate.toFixed(0)}FPS`

    lastTime = nowTime
    lastFrames = nowFrames
  }, 1000)
}
