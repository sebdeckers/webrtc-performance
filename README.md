# webrtc-performance ⏱

Measuring the performance of screen sharing via WebRTC

## Usage

```
npm start
```

## Benchmark Data

📊 [Screen Sharing Performance](https://docs.google.com/spreadsheets/d/18cBbM6XVrUNhaT9EdGfmaug1cDw7jhuT-lTROlCj6X8/edit#gid=0)

## Findings

- Chrome capturer
  - MacOS performance is 1080p22 on octocore i7
  - Windows performance is 1080p47 on dualcore i5
    - Possibly software limited on Windows due to CPU throttling at max 50% of single core [crbug:466879](https://bugs.chromium.org/p/chromium/issues/detail?id=466879)
- Full screen capture vs application window
  - MacOS: Full screen is faster in Chrome and much faster in OBS
  - Windows: Application window is faster in Chrome but fails in OBS for hardware accelerated content
- Other OS APIs exist to enable better performance
  - [Windows 8 Desktop Duplication API](https://msdn.microsoft.com/en-us/library/windows/desktop/hh404487(v=vs.85).aspx)
  - [OBS display capturer for MacOS](https://github.com/jp9000/obs-studio/blob/master/plugins/mac-capture/mac-display-capture.m#L6)

## Settings

### Firefox

<about:config> Custom Settings

```
devtools.sourcemap.locations.enabled # true
media.getusermedia.screensharing.enabled # true
media.getusermedia.screensharing.allowed_domains # add localhost
```

### Chrome

Required Chrome Extension to enable screen sharing permission
- [Install: Screen Capturing](https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk)

## References

- <https://webrtc.github.io/samples/>
- <https://www.webrtc-experiment.com/>
- <https://webrtc.github.io/samples/src/content/capture/video-contenthint/>
- <https://mozilla.github.io/webrtc-landing/gum_test.html>
